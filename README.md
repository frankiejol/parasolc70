# README #

Aquest és el repositori font de la revista Parasolc 70.
Has de fer 'Log in' per poder editar arxius.


## On trobo els arxius que he de modificar ? ##

Escull la icona 'Source' de les que apareixen verticalment a l'esquerra. 
Veuràs un llistat de directoris segons els temes de la revista.
Clica sobre el directori o sobre l'arxiu que vols editar.
Clica sobre 'Edit' a la part superior a la dreta.
Una vegada fets els canvis clica sobre 'Commit' abaix a la dreta.
S'obrira una finestra que et demana si vols posar un comentari al canvi fet. 
Torna a clicar a 'Commit'.
Per últim, podràs veure els canvis fets a l'arxiu.

## Quins arxius he de modificar ? ##

Pots modificar els arxius .tex i les imatges. 

## Hi han molts salts de línia on no toca, els he d'arreglar ? ##

Els únics salts de línia que conten són els dobles. Un salt simple
no apareix al document PDF final.

## Hi han molts salts de línia on no toca, els he d'arreglar ? ##

Els únics salts de línia que conten són els dobles. Un salt simple
no apareix al document PDF final.

## Més informació ##

[Fluxe de dades](https://bitbucket.org/frankiejol/parasolc70/wiki/Fluxe%20de%20dades)