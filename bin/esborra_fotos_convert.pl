#!/usr/bin/perl

use warnings;
use strict;

use BerkeleyDB;
use Cwd;
use Getopt::Long;

use Carp qw(confess);

my $DEBUG;

{
	my $help ;
	GetOptions( help => \$help , debug => \$DEBUG );
	if ($help) {
		print "$0 [--help] [--debug]\n";
		exit;
	}
}

###################################################################
my $HOME = getcwd;
$HOME =~ s{(.*parasolc\d+).*}{$1};
my $FILE_DB = $HOME."/var/fotos.db";
my $FILE_CONFIG = $HOME."/etc/fotos_color.conf";
my @SECCIO;

if ( ! -d "$HOME/var") {
		mkdir "$HOME/var" or die "No puc crear $HOME/var";
}

my %FOTO;
my %COLOR;

my $DB = tie %FOTO , 'BerkeleyDB::Hash'
			, -Filename => $FILE_DB
			, -Flags => DB_CREATE
			;

my $FOTOS_ESBORRADES = 0;
my $MTIME_MAKEFILE;
my $NOUS_TEX = 0;

#############################################
sub esborra_foto_output {
	my $foto = shift or die "Falta foto";
	for my $seccio (@SECCIO) {
        next if ! -e "$HOME/output/$seccio/$foto";
		print "rm $seccio/$foto\n";
		unlink "$HOME/output/$seccio/$foto" or warn "$! $seccio/$foto";
	}
	$FOTOS_ESBORRADES++;
}

sub troba_fotos {
    chdir $HOME or die "No puc chdir $HOME";
    
    open my $find,'-|',"find . -type f"  or die $!;
    while (<$find>) {
    	next if /^output/ || m{^./output};
		if (/\.tex$/) {
			$NOUS_TEX++ if mtime($_)>$MTIME_MAKEFILE;
		}
    	next if !m{/img/.*(gif|png|jpg|jpeg)$}i;
		s{^\.\/}{};
		chomp;
		my $tipus = 'bn';
		$tipus = 'c' if $COLOR{$_};
		if (!defined $FOTO{$_} || $FOTO{$_} ne $tipus ) {
			print "**** ".($FOTO{$_} or '<null>')." $tipus "	if $DEBUG;
			$FOTO{$_} = $tipus;
			esborra_foto_output($_);
		}
    	print "$_\n"	if $DEBUG;
    }
    close $find;
}

sub parse_config {
	open my $config,'<',$FILE_CONFIG or die "$! $FILE_CONFIG";
	while (my $file =<$config>) {
			$file =~ s/#.*//;
			next if $file !~ /\w/;
			chomp $file;
			$COLOR{$file}++;
			warn "Foto repetida $_" if $COLOR{$file}>2;
	}
	close $config;
}

sub genera_makefile {
	my ($dir) = $0 =~ m{(.*)/};
	$dir = '' if !$dir;
	open my $gen,'-|',"$dir/genera_makefile.pl"
				or die "No puc generar makefile\n";
	while (<$gen>) {
			print;
	}
	close $gen;
}

sub busca_seccions {
	my $dir_output = "$HOME/output";
	opendir my $dir,$dir_output or do {
			warn "$! $dir_output";
			return;
	};
	while (my $file = readdir $dir) {
		next if $file =~ m{\.};
		push @SECCIO,($file);
	}
	closedir $dir;
}

sub mtime {
	my $arxiu = shift or die $!;
	chomp $arxiu;
	my @stat = stat($arxiu) or do {
		  warn "No puc mtime $arxiu: $!";
		  return 0;
	};

	return $stat[9];
}
##############################################

busca_seccions();
parse_config();
$MTIME_MAKEFILE = mtime("Makefile");
troba_fotos();
warn "Nous Tex $NOUS_TEX" if $NOUS_TEX;
genera_makefile()	if $FOTOS_ESBORRADES || $NOUS_TEX;
