#!/usr/bin/perl -w

use strict;
use Carp qw(confess);
use Cwd;
use File::Copy;
use Image::Size;
use Getopt::Long;

my @SECCIONS;
my %EXT_IMG = map { $_ => 1 } qw (jpg png gif);
my @TIPUS = qw(lowres paper);

my %NODEP = map { $_ => 1 } qw ("portada/index.tex");

my %EXT_TEX = ( tex => 1);
my %EXT = map { $_ => 1 } keys %EXT_IMG,keys %EXT_TEX;

my ($REVISTA) = obte_revista_actual();

my $DPI_LOWRES = 150;
my $QUALITY_LOWRES = 60;
my $WIDTH_LOWRES = "640";

my $BW_LOWRES = 0;

my $QUALITY_HIRES = 100;

my $IMG_WIDTH = 340;
my $CONF_COLOR = "etc/fotos_color.conf";

{
    my $help;
    GetOptions ( 'dpi-lowres=s' => \$DPI_LOWRES
            ,'width-lowres=s' => \$WIDTH_LOWRES
            ,help => \$help);

    if ($help) {
        print "$0 [--dpi-lowres=$DPI_LOWRES] [--width-lowres=$WIDTH_LOWRES]\n";
        exit;
    }
}


my %FOTO_COLOR;

my %FET;

sub busca_seccions {
	my ($tex) = (shift or "$REVISTA.tex");
	open P_TEX,"<$tex" or die "$! $tex";
	while (<P_TEX>) {
		s/%.*//;
		my ($type,$include) = m[\\(include|input)\{(.*)/];
		next if ! $include;
        next if /portada/;
		push @SECCIONS, ( $include); 
	}
	close P_TEX;
}

sub obte_revista_actual {
    opendir my $ls, '.' or die $!;
    while (my $file = readdir $ls) {
        return $1 if $file =~ /^(parasolc\d+).tex$/;
    }
    close $ls;
    die "No trobo cap parasolcXX.tex";
}
sub ext {
	my $file = shift;
	my ($ext) = $file =~ /\.(\w+)$/;
	confess "L'arxiu '$file' no te extensio" if ! $ext;
	return lc($ext);
}

sub transforma_img {
	my ($file,$tipus) = @_;
	my $file_lowres = "output/$tipus/$file";

	die if $FET{$tipus}->{$file_lowres}++;

	my ($dir) =$file_lowres =~ m{(.*)/};
	$dir = $dir;
	die "No trobo el dir de $file_lowres" if !$dir;

	my ($x) = imgsize($file) or die "No trobo mida de '$file'";
	die "No trobo mida de '$file'" if !defined $x;
	my $width = '';
    my $width_lowres = $WIDTH_LOWRES;
    $width_lowres /= 2 if $file =~ /index.png/;
	$width = " -geometry $width_lowres"
		if $x> $WIDTH_LOWRES*1.1; 

	my $options = "";
	if ($tipus eq 'lowres'){
			$options = 
					" -density $DPI_LOWRES $width -quality $QUALITY_LOWRES ";
	} 
#	warn "$file ".($FOTO_COLOR{$file} or 'bw')."\n";
    if ($tipus eq 'bw' || $tipus eq 'paper' || $tipus eq 'lowres') {
    
			$options .= "-set colorspace RGB -colorspace GRAY +contrast -modulate 110"
                if $tipus eq 'bw' 
                        || ( ($tipus eq 'paper' || $tipus eq 'lowres') && !$FOTO_COLOR{$file});
	} else {
            die "Unknown tipus '$tipus' for $file";
    }
	print "$file_lowres: $file";

#	Aixo no cal per que lo de les fotos color es genera des del makefile
#	print " etc/fotos_color.conf" if $tipus eq 'paper';
	print "\n"
			."\tmkdir -p $dir "
			." ; convert $file $options $file_lowres\n"
			."\n"
	;
	return $file_lowres;
}

sub transforma_tex {
	my ($file,$tipus) = @_;
	my $file_lowres = "output/$tipus/$file";

	die "Repetit $file_lowres" if $FET{$tipus}->{$file_lowres}++;

	print "$file_lowres: $file\n";
	my ($dir) =$file_lowres =~ m{(.*)/};

    if ($file =~ /^parasolc/) {
            print "\t./bin/crea_tex_lowres.pl --$tipus $file\n\n";
    } else {
            print "\tmkdir -p $dir ; cp $file output/$tipus/$file\n\n"
    }
	return $file_lowres;
}

sub transforma {
	my ($file,$tipus) = @_;
	my $ext = ext($file);
	if ($EXT_IMG{$ext}) {
		return transforma_img($file,$tipus);
	} elsif ($EXT_TEX{$ext}) {
		return transforma_tex($file,$tipus);
	} else {
		die "Unknown extension '$ext' for '$file'\n";
	}
}

#sub bw {
#	return transforma(@_,"bw");
#}

sub lowres {
		return transforma(@_,"lowres");
}

sub paper{
        return transforma(@_,"paper");
}

sub print_groups {
	my ($item,$nom) = (@_,"");
	for my $ext (sort keys %$item) {
		print uc("$ext$nom")."=";
		print join " ",sort @{$item->{$ext}};
		print "\n\n";
	}
}

sub target_pdf {
	my ($pdf,$dep,$nom) = (@_,"") or die $!;

    my ($dir,$file) = $pdf =~ m{(.*)/(.*)};

    if (!$dir || !$file) {
        $dir = '.';
        $file = $pdf;
    } else {
        print "$dir/papertex.cls: papertex.cls\n"
                ."\tcd $dir && cp ../../papertex.cls .\n"
                ."\n";
        for (qw(index.tex portada.tex)) {
              print "$dir/portada/$_: portada/$_\n"
                ."\t mkdir -p $dir/portada ; cp portada/$_ $dir/portada/$_\n"
                ."\n";
        }
    }

	print "$pdf.pdf: ";
	for my $ext (sort keys %$dep) {
		print '$('.uc("$ext$nom").") $dir/portada/index.tex $dir/portada/portada.tex "
	}
    print " $dir/portada/img/portada.png ";

	print join " ",map { "$dir/$_/main.tex" } @SECCIONS;
	print " $dir/papertex.cls $pdf.tex";
	print "\n"
		."\tcd $dir \&\& ";

	print "./bin/fix_inputs.pl && " if $dir eq '.';

	print " pdflatex $file.tex\n"
		."\n";

    if ($dir ne '.') {
        for (@SECCIONS) {
            print "$dir/$_/main.tex: $_/main.tex\n"
            ."\t"
            ."mkdir -p $dir/$_"
            ."; cp $_/main.tex $dir/$_/main.tex\n"
            ."\n";
        }
    }
}

sub make_init {

	print "ALL: $REVISTA.pdf ".(join(" ",map { "output/$_/$REVISTA.pdf" } @TIPUS ))
		."\n\n"
		."lowres: output/lowres/$REVISTA.pdf\n"
		."\n\n"
		."paper: output/paper/$REVISTA.pdf\n"
        ;

    my $esborrany = "$ENV{HOME}/Dropbox/$REVISTA/$REVISTA.esborrany.pdf";
    my $todo= "$ENV{HOME}/Dropbox/$REVISTA/TODO.txt";
    my $log= "$ENV{HOME}/Dropbox/$REVISTA/LOG.txt";

    print "esborrany: $esborrany $todo\n";

    print "$esborrany: output/lowres/$REVISTA.pdf\n"
        ."\tcp output/lowres/$REVISTA.pdf $esborrany\n"
        ."\n";

    print "$todo: TODO.txt\n"
        ."\tcp TODO.txt $todo\n"
        ."\n";
#    print "$log: .git\n"
#        ."\tgit log | unix2dos > $log"
#        ."\n";

	for my $tipus (@TIPUS) {

		print "output/$tipus/$REVISTA.tex: $REVISTA.tex .git etc/status.txt\n"
		."\t./bin/crea_tex_lowres.pl --tipus=$tipus $REVISTA.tex\n"
		."\n";
	}

	papertex();

}

sub portada {
    print "portada/index.tex: \$(TEX) ";
    for (@SECCIONS) {
        print " $_/main.tex" if !/portada/;
    }
    print "\n"
        ."\t./bin/fix_inputs.pl && ./bin/busca_noticies.pl\n"
        ."\n";
}

sub papertex  {
	print "papertex.cls: papertex.ins papertex.dtx\n"
		."\trm -f papertex.cls *.aux ; latex papertex.ins\n"
		."\n";
}


sub make_clean {
    print "clean:\n"
            ."\t rm -rf $REVISTA.pdf output/*/$REVISTA.pdf "
			." papertex.cls "
			." *.aux *.log *.out */*.aux */*.log ";
    for (@SECCIONS) {
        print "$_/main.tex " if !/portada/;
    }
    print "\n";

	print "realclean: clean\n"
			."\t rm -rf output Makefile\n";
}

sub article_color {
	my $file = shift or die $!;
	open ARTICLE,"<$file" or die "$! $file";
	while (<ARTICLE>) {
		m[(includegraphics|image|ThisCenterWallPaper|weatheritem).*\{(.*)/(.*?)\.(jpg|png).*\}]i;
		next if !($2 && $3 && $4);
		my $file = "$2/$3.$4";
		$FOTO_COLOR{$file}++;
	}
	close ARTICLE;
}

sub fotos_color {
     open CONF,"<$CONF_COLOR" or die "$! $CONF_COLOR";
     while (my $file=<CONF>) {
        chomp $file;
		if ($file =~ /\.tex$/) {
				article_color($file);
		} else {
             $FOTO_COLOR{$file}++;
	 	}
     }
     close CONF;
}

sub seccions {
    for my $seccio (@SECCIONS) {
        next if $seccio =~ /portada/;
        print "$seccio/main.tex: ";

        my @files;
        opendir my $ls,$seccio or die "$! $seccio";
        while (my $file = readdir $ls) {
            push @files,("$seccio/$file")
                if $file =~ /\.tex$/ && $file !~ /main.tex$/;
        }
        closedir $ls;

        if (!scalar(@files)) {
            print "\n\techo '' > $seccio/main.tex\n";
        } else {
            print join(" ",@files)
            ."\n"
            ."\tcat ".join(" ",@files)." > $seccio/main.tex"
            ."\n";
        }
    }
}

############################################################


busca_seccions();
fotos_color();

open MAKEFILE,">Makefile" or die $!;
select(MAKEFILE);

make_init();
my (%lowres,%all, %paper);
open FIND,"find ".join(" ",@SECCIONS)." -type f |" or die $!;
while (my $file = <FIND>) {
	chomp $file;
    
	my $ext = ext($file);
	next if !$ext || !$EXT{$ext};

    next if $NODEP{$file};
    next if $file =~ m{portada/index.tex};
    next if $file =~ m{main.tex$};

	push @{$lowres{$ext}},(lowres($file));
#	push @{$bw{$ext}},(bw($file));
    push @{$paper{$ext}},(paper($file));

	push @{$all{$ext}},($file);
}
close FIND;

push @{$lowres{png}},lowres('portada/img/portada.png');
#push @{$bw{png}},bw('portada/img/portada.png');
push @{$paper{png}},paper('portada/img/portada.png');

print_groups(\%lowres,"_LOWRES");
#print_groups(\%bw,"_BW");
print_groups(\%paper,"_PAPER");

print_groups(\%all);
target_pdf($REVISTA,\%all);

portada();

seccions();

target_pdf("output/lowres/$REVISTA",\%lowres,"_LOWRES");
#target_pdf("output/bw/$REVISTA",\%bw,"_BW");
target_pdf("output/paper/$REVISTA",\%paper,"_PAPER");

make_clean();
close MAKEFILE;
