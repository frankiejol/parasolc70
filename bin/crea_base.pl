#!/usr/bin/perl -w

use strict;
use Carp qw(confess);
use Cwd;
use File::Copy;
use Getopt::Long;

# config
my $IMG_BLANK = getcwd."/portada/img/flower.png";
my @ARXIUS_BASE = qw( papertex.ins papertex.dtx .gitignore);

my $EXEMPLE_TEX = "base/exemple.tex";

# globals
my $REVISTA_BASE;
my $REVISTA_NOVA;
my $REVISTA_NOVA_IMG;
my $REVISTA_BASE_IMG;

my $DIR_HOME;
#
my @SECCIONS;

{
	my $help;
	GetOptions( help => \$help,
				'base=s' => \$REVISTA_BASE,
				'nova=s' => \$REVISTA_NOVA
	);
	if ($help) {
		print "$0 [--help] [--base=parasolcXXX] [--nova=parasolcXXX+1]\n";
		exit;
	}
	if ($REVISTA_BASE && ! -d $REVISTA_BASE) {
		die "No trobo el directori per revista base '$REVISTA_BASE'\n";
	}
	if ($REVISTA_NOVA && -d $REVISTA_NOVA) {
		warn "Sobreescribint revista nova '$REVISTA_NOVA'\n";
		print "Vols continuar [s/N]? ";
		my $que = <STDIN>;
		exit if $que !~ /s/i;
	}
}


####################################################################
#
# subs
#

sub te_arxius_base {
	my $dir = shift;
	my ($revista) = $dir =~ m{.*/(parasolc\d+)};
	return -e "$dir/$revista.tex";
}

sub init_dirs {
    my ($home) = $0 =~ m{(.*)bin/.*};
    if ($home =~ m{\./}) {
        my $cwd = getcwd;
        $home =~ s{\.(/.*)}{$cwd$1};
    }
    $REVISTA_BASE = $home;
    $REVISTA_BASE =~ s{/$}{};

    die "Dir de la nova ja existeix : ".revista_nova() if -e revista_nova() ;
}

sub revista_base {
	return $REVISTA_BASE if $REVISTA_BASE;

    init_dirs();
    return $REVISTA_BASE;
}

sub revista_nova {
	return $REVISTA_NOVA if $REVISTA_NOVA;

	my ($nom,$num) = revista_base =~ /(.*?)(\d+)$/;
    die "No trobo nom i num a revista base ".revista_base()
        if !$nom || !$nom;
	$REVISTA_NOVA = $nom.($num+1);

	return $REVISTA_NOVA;
}

#sub revista_nova_img {
#	
#	return $REVISTA_NOVA_IMG if $REVISTA_NOVA_IMG;
#
#	my ($dir) = revista_nova =~ m{.*(parasolc\d+)};
#	
#	$REVISTA_NOVA_IMG="$ENV{HOME}/Dropbox/parasolc/$dir";
#	return $REVISTA_NOVA_IMG;
#}

sub revista_antiga_img {
	return $REVISTA_BASE_IMG if $REVISTA_BASE_IMG;

	my ($dir) = revista_base =~ m{.*(parasolc\d+)};
	
	$REVISTA_BASE_IMG="$ENV{HOME}/Dropbox/parasolc/$dir";
	return $REVISTA_BASE_IMG;
}


sub tex_base {
	my ($tex_base) = revista_base() =~ m{.*/?(parasolc\d+)};
	die "No trobo parasolcXX a ".revista_base() if !$tex_base;
	return revista_base."/$tex_base.tex";
}

sub busca_seccions {
	open P_TEX,"<".tex_base() or die "$! ".tex_base();
	while (<P_TEX>) {
		s/#.*//;
		my ($include) = m[\\include\{(.*)/];
		push @SECCIONS, ( $include) if $include;
	}
	close P_TEX;
}

sub crea_dir {
	my $arxiu = shift or die;
	my ($dir) = $arxiu =~ m{(.*)/};
	print " creant dir $dir\n";
	return if -d $dir;
	mkdir $dir;
	my @items = split m{/},$dir;
	my $dst = shift @items;
	for my $item (@items) {
		$dst .= "/$item";
		next if -d $dst;
		mkdir $dst or confess "$! $dst";
	}
}

sub copia {
	my ($orig,$dst) = @_;
	confess "Falta '$orig'".cwd."\n" if ! -e $orig;
	return if -e $dst;
	crea_dir($dst);
	copy($orig,$dst) or die "$! $orig -> $dst (".getcwd.")";
}

sub crea_img_seccions {
	my $orig = shift;
	
	my ($img) = $orig =~ m{.*/(.*)};
	print "\nCREA_IMG_SECCIONS\n -> $orig\n";
	for my $seccio (@SECCIONS) {
		my $arxiu = revista_nova()."/$seccio/img/$img";
		print "$IMG_BLANK -> $arxiu\n";
		copia($IMG_BLANK,$arxiu);
	}
}

sub crea_img {
	my $arxiu = shift;
	$arxiu = revista_nova()."/$arxiu";
	copia(revista_antiga()."/$IMG_BLANK",$arxiu);
}

sub copia_tex {
	my ($arxiu) = revista_base() =~ /(parasolc\d+)/;
	my ($arxiu_nou) = revista_nova =~ /(parasolc\d+)/;
	copia(revista_base()."/$arxiu.tex",revista_nova."/$arxiu_nou.tex");
}

sub pagesof {
	my $seccio  = shift;
	my $pagesof = 0;
	my $trobat  = 0;

	for (@SECCIONS) {
		$pagesof++;
		if ( $seccio eq $_ ) {
			$trobat++;
			last;
		}
	}

	die "No trobo la seccio $seccio a ".join(",",@SECCIONS)
		if !$trobat;
	return ($pagesof * 100) + 1;
}

sub crea_exemple {
	my $seccio = shift;
	my $pagesof = pagesof($seccio);
	my $seccio_tex = revista_nova()."/$seccio/00_exemple.tex";
	print " Faig $seccio_tex\n";

	my $img_exemple = "$seccio/img/flower.png";

	$seccio =~ s/_/ /g;
	$seccio = "\u$seccio";

	my $seccio_nom = $seccio;
	$seccio_nom = 'Primària' if $seccio =~ /primaria/;

	open TEX,">$seccio_tex" or die "$! $seccio_tex";
	print TEX <<EOF;
%doc: directori/original.doc
\\newpage

\\begin{news}
{2} %columnes
{Titol exemple $seccio_nom}
{Subtitol exemple $seccio}
{$seccio}
{$pagesof} %pagesof


\\noindent\\includegraphics[width=\\columnwidth,keepaspectratio]{$img_exemple}

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

\\authorandplace{Nom i Cognoms}{Curs}
						
\\end{news}

EOF

	close TEX;
}

sub crea_seccions {
	crea_img_seccions("img/index.png");
	crea_img_seccions($IMG_BLANK);
	for my $seccio_nom (@SECCIONS) {
		my $seccio_tex = revista_nova()."/$seccio_nom/$seccio_nom.tex";
		crea_dir($seccio_tex);

		next if $seccio_nom =~ 'portada';

		if ( ! -e $seccio_tex ) {
			print " Blank $seccio_tex\n";
			open SECCIO,">$seccio_tex" or die "$! $seccio_tex";
			print SECCIO "\\input{$seccio_nom/exemple.tex}\n";
			close SECCIO;
		}

		crea_exemple($seccio_nom);
	}
}

sub copia_base {
	for (@ARXIUS_BASE) {
		copia(revista_base()."/$_",revista_nova."/$_");
	}
	copia_tex();
    copia_dir('bin');
    copia_dir('lib');
    copia_dir('docs');
}

sub copia_dir {
    my $dir = shift or die $!;
    my $origen = revista_base()."/$dir";
    my $desti  = revista_nova()."/$dir";
    open my $rsync,,'-|',"rsync -av ".revista_base."/$dir ".revista_nova 
        or die "$! ".revista_base."/$dir";
    while (<$rsync>) {
        print;
    }
    close $rsync;
}

sub crea_etc_fotos_color {
	my $arxiu =  revista_nova()."/etc/fotos_color.conf";
	crea_dir($arxiu);
	return if -e $arxiu;
	open ARXIU,">$arxiu" or die "$! $arxiu";
	print ARXIU "#$arxiu\n"
				."#\n#Fica aqui el path complert a les fotos en color\n";
	print ARXIU "#portada/img/portada.png\n";
	close ARXIU;
	
}

sub crea_etc {
	crea_etc_fotos_color();
}

sub copia_portada {
	my $d_portada = revista_base()."/portada";
	opendir D_PORTADA,$d_portada or die "$! $d_portada";
	while (my $file = readdir D_PORTADA) {
		my $orig = "$d_portada/$file";
		my $dest = revista_nova()."/portada/$file";
		next if -d $orig;
		next if -e $dest;
		next if $orig !~ /\.tex$/;
		print "$orig -> $dest\n";
		crea_dir($dest);
		copy($orig,$dest) or die "$! $orig -> $dest";
	}
	closedir D_PORTADA;
    my $dest = revista_nova()."/portada/img/portada.png";
    my $dir_portada_img = revista_nova()."/portada/img";
    mkdir $dir_portada_img
        if ! -e $dir_portada_img;
    copy($IMG_BLANK,$dest) or die "$! $IMG_BLANK -> $dest";
}

sub crea_status {
#    warn "untested crea status";
    open my $status,'>',revista_nova."/etc/status.txt" or die $!;
    print $status "alpha 0";
    close $status;

    open my $todo,'>',"TODO.txt" or die $!;
    close $todo;
}

#############################################################################
#
# main
warn "Creant la base per la revista ".revista_nova()."\n";
init_dirs;
busca_seccions();
#crea_img("portada/portada.png");
copia_base();
copia_portada();
crea_etc();
crea_seccions();
crea_status();
